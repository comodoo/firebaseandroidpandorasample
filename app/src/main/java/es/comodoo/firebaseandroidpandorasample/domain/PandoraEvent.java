package es.comodoo.firebaseandroidpandorasample.domain;

public class PandoraEvent extends DomainClass{
    private String timestamp;
    private String agente;
    private int agentstatus;
    private String alert_name;
    private String alert_description;
    private String alert_text_severity;
    private String alert_threshold;
    private int alert_times_fired;
    private String data;
    private String modulo;
    private String texto;
    private String comentarios;

    public PandoraEvent() {
    }


    public PandoraEvent(String agente, int agentstatus, String alert_description, String alert_name,
                        String alert_text_severity, String alert_threshold, int alert_times_fired, String comentarios,
                        String data, String modulo, String texto, String timestamp) {
        this.agente = agente;
        this.agentstatus = agentstatus;
        this.alert_description = alert_description;
        this.alert_name = alert_name;
        this.alert_text_severity = alert_text_severity;
        this.alert_threshold = alert_threshold;
        this.alert_times_fired = alert_times_fired;
        this.comentarios = comentarios;
        this.data = data;
        this.modulo = modulo;
        this.texto = texto;
        this.timestamp = timestamp;
    }

    public String getAgente() {
        return agente;
    }

    public void setAgente(String agente) {
        this.agente = agente;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getAgentstatus() {
        return agentstatus;
    }

    public void setAgentstatus(int agentstatus) {
        this.agentstatus = agentstatus;
    }

    public String getAlert_description() {
        return alert_description;
    }

    public void setAlert_description(String alert_description) {
        this.alert_description = alert_description;
    }

    public String getAlert_name() {
        return alert_name;
    }

    public void setAlert_name(String alert_name) {
        this.alert_name = alert_name;
    }

    public String getAlert_text_severity() {
        return alert_text_severity;
    }

    public void setAlert_text_severity(String alert_text_severity) {
        this.alert_text_severity = alert_text_severity;
    }

    public String getAlert_threshold() {
        return alert_threshold;
    }

    public void setAlert_threshold(String alert_threshold) {
        this.alert_threshold = alert_threshold;
    }

    public int getAlert_times_fired() {
        return alert_times_fired;
    }

    public void setAlert_times_fired(int alert_times_fired) {
        this.alert_times_fired = alert_times_fired;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

}
