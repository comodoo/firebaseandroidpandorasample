package es.comodoo.firebaseandroidpandorasample.domain;


public class PandoraAgent extends DomainClass{

    private String agentName;
    private int agentStatus;

    public PandoraAgent() {

    }

    public PandoraAgent(String agentName, int agentStatus) {
        this.agentName = agentName;
        this.agentStatus=agentStatus;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public int getAgentStatus() {
        return agentStatus;
    }

    public void setAgentStatus(int agentStatus) {
        this.agentStatus = agentStatus;
    }
}
