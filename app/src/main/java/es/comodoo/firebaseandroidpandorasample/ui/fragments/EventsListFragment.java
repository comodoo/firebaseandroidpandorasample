package es.comodoo.firebaseandroidpandorasample.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import es.comodoo.firebaseandroidpandorasample.R;
import es.comodoo.firebaseandroidpandorasample.domain.PandoraEvent;
import es.comodoo.firebaseandroidpandorasample.ui.adapters.EventAdapter;
import es.comodoo.firebaseandroidpandorasample.ui.listeners.PandoraItemListener;
import es.comodoo.firebaseandroidpandorasample.ui.viewholders.EventViewHolder;


public class EventsListFragment extends ElementListFragment implements PandoraItemListener {

    private static final String TAG = "EventsListFragment";
    public static final String FIREBASE_ALERTS_BRANCH = "Alerts";
    public static final String CHILD_KEY = "childKey";


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setLayoutManager(new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL));
        getRecyclerView().setLayoutManager(getLayoutManager());

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String childKey = bundle.getString(CHILD_KEY);
            getActivity().setTitle(childKey);
            EventAdapter adapter = new EventAdapter(
                    PandoraEvent.class,
                    R.layout.item_pandora_event,
                    EventViewHolder.class,
                    mFirebaseDatabaseReference.child(FIREBASE_ALERTS_BRANCH + "/" + childKey),
                    progressBar, this);

            setFirebaseRecyclerAdapter(adapter);

            getFirebaseRecyclerAdapter().registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onItemRangeInserted(int positionStart, int itemCount) {
                    super.onItemRangeInserted(positionStart, itemCount);
                }
            });
            getRecyclerView().setAdapter(getFirebaseRecyclerAdapter());
        }

    }


    @Override
    public void onElementItemClick(View view, String elementKey) {
       cargarDetalleEventFragment(elementKey);
    }
    private void cargarDetalleEventFragment(String evento) {
        Fragment fragment = new DetallePandoraEventFragment();

        Bundle bundle = new Bundle();
        bundle.putString(DetallePandoraEventFragment.KEY, evento);
        fragment.setArguments(bundle);
        FragmentTransaction ft = this.getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment).addToBackStack("detalle").commit();
    }
}