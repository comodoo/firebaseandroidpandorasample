package es.comodoo.firebaseandroidpandorasample.ui.fragments;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.comodoo.firebaseandroidpandorasample.R;
import es.comodoo.firebaseandroidpandorasample.domain.PandoraEvent;


public class DetallePandoraEventFragment extends Fragment {

    public static final String KEY ="element_key";
    public static String TAG="DetalleEvento";

    @BindView(R.id.moduloTV)
    TextView moduloTV;
    @BindView(R.id.fechaEventoTV)
    TextView fechaEventoTV;
    @BindView(R.id.estadoAgenteTV)
    TextView estadoAgenteTV;
    @BindView(R.id.alertNameTV)
    TextView alertNameTV;
    @BindView(R.id.alertDescriptionTV)
    TextView alertDescriptionTV;
    @BindView(R.id.alertTextSeverityTV)
    TextView alertTextSeverityTV;
    @BindView(R.id.dataTV)
    TextView dataTV;
    @BindView(R.id.alertTimesFiredTV)
    TextView alert_times_firedTV;

    String element_key;
    PandoraEvent event;

    private DatabaseReference dbReference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View rootView = inflater.inflate(R.layout.detalle_pandrora_event, container, false);
        ButterKnife.bind(this, rootView);


        Bundle bundle=this.getArguments();
        if(bundle!=null){
            element_key=bundle.getString(KEY);
            dbReference= FirebaseDatabase.getInstance().getReferenceFromUrl(element_key);
            dbReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    event = dataSnapshot.getValue(PandoraEvent.class);
                    actualizarVistas(event);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Snackbar.make(rootView,"Error!!!!!",Snackbar.LENGTH_SHORT).show();
                    Log.e(TAG, "Error!", databaseError.toException());
                }
            });


        }
        return rootView;
    }

    private void actualizarVistas(PandoraEvent event) {

        getActivity().setTitle(event.getAgente());
        fechaEventoTV.setText(event.getTimestamp());
        moduloTV.setText(event.getModulo());
        dataTV.setText(event.getData());
        estadoAgenteTV.setText(""+event.getAgentstatus());
        alertNameTV.setText(event.getAlert_name());
        alertDescriptionTV.setText(event.getAlert_description());
        alertTextSeverityTV.setText(event.getAlert_text_severity());
        alert_times_firedTV.setText(""+event.getAlert_times_fired());


    }

}
