package es.comodoo.firebaseandroidpandorasample.ui.adapters;

import android.support.v4.app.Fragment;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

import es.comodoo.firebaseandroidpandorasample.domain.PandoraAgent;
import es.comodoo.firebaseandroidpandorasample.ui.listeners.PandoraItemListener;
import es.comodoo.firebaseandroidpandorasample.ui.viewholders.AgentViewHolder;

public class AgentAdapter extends FirebaseRecyclerAdapter<PandoraAgent, AgentViewHolder> /*implements PandoraItemListener */{


    public static String TAG = "PandoraAgentAdapter";
    ProgressBar progressBar;
    Fragment fragment;



    public AgentAdapter(Class<PandoraAgent> modelClass, int modelLayout,
                        Class<AgentViewHolder> viewHolderClass, DatabaseReference ref,
                        ProgressBar progressBar, Fragment fragment) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.progressBar = progressBar;
        this.fragment=fragment;

    }
    public AgentAdapter(Class<PandoraAgent> modelClass, int modelLayout,
                        Class<AgentViewHolder> viewHolderClass, DatabaseReference ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);
    }

    @Override
    public AgentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    protected void populateViewHolder(final AgentViewHolder viewHolder,
                                      final PandoraAgent model, int position) {
        final DatabaseReference eventRef = getRef(position);
        final String deviceKey = eventRef.getKey();

        if (progressBar != null)
            progressBar.setVisibility(ProgressBar.INVISIBLE);
        viewHolder.setData(model, eventRef);
        viewHolder.setListener((PandoraItemListener) fragment);


    }

}