package es.comodoo.firebaseandroidpandorasample.ui.adapters;

import android.support.v4.app.Fragment;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

import es.comodoo.firebaseandroidpandorasample.domain.PandoraEvent;
import es.comodoo.firebaseandroidpandorasample.ui.listeners.PandoraItemListener;
import es.comodoo.firebaseandroidpandorasample.ui.viewholders.EventViewHolder;

public class EventAdapter extends FirebaseRecyclerAdapter<PandoraEvent, EventViewHolder> /*implements PandoraItemListener */{


    public static String TAG = "PandoraEventAdapter";
    ProgressBar progressBar;
    Fragment fragment;

    public EventAdapter(Class<PandoraEvent> modelClass, int modelLayout,
                        Class<EventViewHolder> viewHolderClass, DatabaseReference ref,
                        ProgressBar progressBar, Fragment fragment) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.progressBar = progressBar;
        this.fragment = fragment;

    }

    public EventAdapter(Class<PandoraEvent> modelClass, int modelLayout,
                        Class<EventViewHolder> viewHolderClass, DatabaseReference ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    protected void populateViewHolder(final EventViewHolder viewHolder,
                                      final PandoraEvent model, int position) {
        final DatabaseReference eventRef = getRef(position);
        final String deviceKey = eventRef.getKey();

        if (progressBar != null)
            progressBar.setVisibility(ProgressBar.INVISIBLE);
        viewHolder.setData(model, eventRef);
        viewHolder.setListener((PandoraItemListener) fragment);


    }

}