package es.comodoo.firebaseandroidpandorasample.ui.listeners;

import android.view.View;


public interface PandoraItemListener {

    public void onElementItemClick(View view, String elementKey);
}
