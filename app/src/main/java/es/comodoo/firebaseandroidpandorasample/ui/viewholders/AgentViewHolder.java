package es.comodoo.firebaseandroidpandorasample.ui.viewholders;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.comodoo.firebaseandroidpandorasample.R;
import es.comodoo.firebaseandroidpandorasample.domain.PandoraAgent;
import es.comodoo.firebaseandroidpandorasample.ui.listeners.PandoraItemListener;


public class AgentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


    private static final String TAG ="PandoraAgentViewHolder" ;
    @BindView(R.id.imageestado)
    ImageView imageestado;
    @BindView(R.id.nombreAgente)
    TextView nombreAgente;

    DatabaseReference agentReference;
    private Context context;
    PandoraItemListener ckickListener;
    View v;

    public AgentViewHolder(View v) {

        super(v);
        context = v.getContext();
        ButterKnife.bind(this, v);
        this.v=v;
    }



    public void setData(PandoraAgent data, DatabaseReference agentreference) {
        nombreAgente.setText(data.getAgentName());

        int badgeColor;
        switch (data.getAgentStatus()){
            case 0:
                badgeColor= ContextCompat.getColor(context, R.color.green);
                break;
            case 1:
                badgeColor=ContextCompat.getColor(context, R.color.red);
                break;
            default:
                badgeColor=ContextCompat.getColor(context, R.color.orange);
                break;
        }
        imageestado.setColorFilter(badgeColor);

        this.agentReference =agentreference;

    }


    public void setListener(PandoraItemListener listener){
        this.ckickListener =listener;
        v.setOnClickListener(this);
    }

    public View getV() {
        return v;
    }

    @Override
    public void onClick(View view)
    {
            ckickListener.onElementItemClick(v, agentReference.getKey());
    }
}

