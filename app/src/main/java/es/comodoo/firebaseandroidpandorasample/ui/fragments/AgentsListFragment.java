package es.comodoo.firebaseandroidpandorasample.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import es.comodoo.firebaseandroidpandorasample.R;
import es.comodoo.firebaseandroidpandorasample.domain.PandoraAgent;
import es.comodoo.firebaseandroidpandorasample.ui.adapters.AgentAdapter;
import es.comodoo.firebaseandroidpandorasample.ui.listeners.PandoraItemListener;
import es.comodoo.firebaseandroidpandorasample.ui.viewholders.AgentViewHolder;


public class AgentsListFragment extends ElementListFragment implements PandoraItemListener {

    static public String AGENT_STATUS = "AgentsStatus";
    private static final String TAG = "AgentsListFragment ";
    private static final String TITLE = "Agentes";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setLayoutManager(new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL));
        getRecyclerView().setLayoutManager(getLayoutManager());

        getActivity().setTitle(TITLE);

        AgentAdapter adapter = new AgentAdapter(
                PandoraAgent.class,
                R.layout.item_pandora_agent,
                AgentViewHolder.class,
                mFirebaseDatabaseReference.child(AGENT_STATUS),
                progressBar, this);
        setFirebaseRecyclerAdapter(adapter);

        getFirebaseRecyclerAdapter().registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
            }
        });
        getRecyclerView().setAdapter(getFirebaseRecyclerAdapter());

    }

    @Override
    public void onElementItemClick(View view, String elementKey) {
        cargarListaDeEventos(elementKey);
    }
    private void cargarListaDeEventos(String nombreAgente) {
        Fragment fragment = new EventsListFragment();

        Bundle bundle = new Bundle();
        bundle.putString(EventsListFragment.CHILD_KEY, nombreAgente);
        fragment.setArguments(bundle);
        FragmentTransaction ft = this.getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment).addToBackStack("eventos").setBreadCrumbTitle("HOla").commit();
    }

}

