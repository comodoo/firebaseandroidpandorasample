package es.comodoo.firebaseandroidpandorasample.ui.viewholders;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.comodoo.firebaseandroidpandorasample.R;
import es.comodoo.firebaseandroidpandorasample.domain.PandoraEvent;
import es.comodoo.firebaseandroidpandorasample.ui.listeners.PandoraItemListener;

public class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


    private static final String TAG ="PandoraEventViewHolder" ;
    @BindView(R.id.fechaEventoTV)
    TextView fechaEventoTV;
    @BindView(R.id.moduloTV)
    TextView moduloTV;
    @BindView(R.id.textoTV)
    TextView textoTV;
    @BindView(R.id.imagehot)
    ImageView imageViewIV;

    DatabaseReference eventRef;
    private Context context;

    PandoraItemListener ckickListener;
    View v;

    public EventViewHolder(View v) {

        super(v);
        context = v.getContext();
        ButterKnife.bind(this, v);
        this.v=v;
    }



    public void setData(PandoraEvent data, DatabaseReference eventRef) {
        fechaEventoTV.setText(data.getTimestamp());
        moduloTV.setText(data.getModulo());
        textoTV.setText(data.getTexto());
        int badgeColor;
        switch (data.getAgentstatus()){
            case 0:
                badgeColor= ContextCompat.getColor(context, R.color.green);
                break;
            case 1:
                badgeColor=ContextCompat.getColor(context, R.color.red);
                break;
            default:
                badgeColor=ContextCompat.getColor(context, R.color.orange);
                break;
        }
        imageViewIV.setColorFilter(badgeColor);
        this.eventRef=eventRef;

    }


    public void setListener(PandoraItemListener listener){
        this.ckickListener =listener;
        v.setOnClickListener(this);
    }

    public View getV() {
        return v;
    }

    @Override
    public void onClick(View view) {
            ckickListener.onElementItemClick(v,eventRef.toString());
    }
}

