package es.comodoo.firebaseandroidpandorasample.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.comodoo.firebaseandroidpandorasample.R;
import es.comodoo.firebaseandroidpandorasample.domain.DomainClass;

public class ElementListFragment extends Fragment {


    private RecyclerView.LayoutManager layoutManager;

    @BindView(R.id.fragmentRecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    DatabaseReference mFirebaseDatabaseReference=FirebaseDatabase.getInstance().getReference();

    FirebaseRemoteConfig mFirebaseRemoteConfig;

    private FirebaseRecyclerAdapter<? extends DomainClass, ? extends RecyclerView.ViewHolder> firebaseRecyclerAdapter;

    public ElementListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_element_list, container, false);
        ButterKnife.bind(this, rootView);

//        Application.getNetComponent(getContext()).inject(this);
        recyclerView.setHasFixedSize(true);

        return rootView;
    }


    public FirebaseRecyclerAdapter<? extends DomainClass, ? extends RecyclerView.ViewHolder> getFirebaseRecyclerAdapter() {
        return firebaseRecyclerAdapter;
    }

    public void setFirebaseRecyclerAdapter(FirebaseRecyclerAdapter<? extends DomainClass, ? extends RecyclerView.ViewHolder> firebaseRecyclerAdapter) {
        this.firebaseRecyclerAdapter = firebaseRecyclerAdapter;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (firebaseRecyclerAdapter != null) {
            firebaseRecyclerAdapter.cleanup();
        }
    }


    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return layoutManager;
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public DatabaseReference getmFirebaseDatabaseReference() {
        return mFirebaseDatabaseReference;
    }

    public void setmFirebaseDatabaseReference(DatabaseReference mFirebaseDatabaseReference) {
        this.mFirebaseDatabaseReference = mFirebaseDatabaseReference;
    }

    public FirebaseRemoteConfig getmFirebaseRemoteConfig() {
        return mFirebaseRemoteConfig;
    }

    public void setmFirebaseRemoteConfig(FirebaseRemoteConfig mFirebaseRemoteConfig) {
        this.mFirebaseRemoteConfig = mFirebaseRemoteConfig;
    }


}
