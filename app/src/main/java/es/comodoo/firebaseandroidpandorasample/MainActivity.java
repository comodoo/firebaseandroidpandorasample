package es.comodoo.firebaseandroidpandorasample;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.comodoo.firebaseandroidpandorasample.ui.activities.BaseActivity;
import es.comodoo.firebaseandroidpandorasample.ui.activities.EmailPasswordActivity;
import es.comodoo.firebaseandroidpandorasample.ui.fragments.AgentsListFragment;

public class MainActivity extends BaseActivity implements FirebaseAuth.AuthStateListener {

    private static final String TAG = "MainActivity";

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener=this;
    }

    public void launchSignActivity() {

        startActivity(new Intent(this, EmailPasswordActivity.class));
    }


    private void buildUserInterface() {
        toolbar.setTitle("Agentes");
        cargarListaDeAgentes();

    }

    private void cargarListaDeAgentes() {
        Fragment fragment = new AgentsListFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment).commit();
    }



    // [START on_start_add_listener]
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    // [END on_start_add_listener]

    // [START on_stop_remove_listener]
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
            buildUserInterface();
        } else {
            Log.d(TAG, "onAuthStateChanged:signed_out");
            launchSignActivity();
        }
    }
}
