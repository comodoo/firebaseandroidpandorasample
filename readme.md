# FirebaseAndroidPandoraSample 

Example of Pandora FMS, Firebase and Android integration to get a real-time monitoring system

## Requirements
To test the content of this repository it is necessary to have a PandoraFMS service and configure some services in Firebase.

* [Firebase](https://firebase.google.com/) - Firebase Services
* [PandoraFMS](https://pandorafms.org) - OpenSource Monitoring Software

## Setup instructions
Please read [guide.pdf](https://bitbucket.org/comodoo/firebaseandroidpandorasample/src/4d750c411be2e02813d38fa550387d6fc576f8f0/guide.pdf)

## Built With

* [Firebase](https://firebase.google.com/) - Firebase Services
* [Butterknife](http://jakewharton.github.io/butterknife/) - Field and method binding for Android views
* [Dagger](http://square.github.io/dagger/) - Dependency injector for Android and Java

## Contributing

Please read [CONTRIBUTING.md](https://bitbucket.org/comodoo/firebaseandroidpandorasample/src/4d750c411be2e02813d38fa550387d6fc576f8f0/Contributing.md) for details on our code of conduct, and the process for submitting pull requests to us.


## Authors

Comodoo Desarrollo de Software - [Comodoo](https://www.comodoo.es/)